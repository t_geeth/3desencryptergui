﻿/*
 * Created by SharpDevelop.
 * User: geetht
 * Date: 2018-08-09
 * Time: 2:52 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using  System.Security.Cryptography;

namespace TDesEncrypter
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			
			
			const string defaultKey = "112233445566778899011122334455667788990111223344";
			txtKey.Text = defaultKey;
			
		}
		void EncryptClick(object sender, EventArgs e)
		{
			if(txtIv.TextLength != 16)
			{
				MessageBox.Show("IV must be 16 hex");
				return;
			}
			if(txtKey.TextLength != 48)
			{
				MessageBox.Show("Key must be 48 hex");
				return;
			}
			if(txtPlain.TextLength == 0)
			{
				MessageBox.Show("No text!");
				return;
			}
			
			byte[] key = ParseHex(txtKey.Text);
			byte[] iv = ParseHex(txtIv.Text);
			byte[] plainText = Encoding.UTF8.GetBytes(txtPlain.Text);
			
			byte[] encrypted = Encrypt(key,iv,plainText);
			string hexEnc = ToHex(encrypted);
			
			txtEnc.Text = hexEnc;
			txtCombined.Text = txtIv.Text + ";" + hexEnc;
			txtEncB64.Text = Convert.ToBase64String(encrypted);
		}
		
		public byte[] ParseHex(String hex)
		{
		    byte[] raw = new byte[hex.Length / 2];
		    for (int i = 0; i < raw.Length; i++)
		    {
		        raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
		    }
		    return raw;
		}
		
		public string ToHex(byte[] data)
		{
			string txt = "";
			
			foreach(byte b in data)
			{
				txt += b.ToString("x2");
			}
			return txt;
		}
		
		public byte[] Encrypt(byte[] key, byte[] iv, byte[] plain)
		{
			var enc = new TripleDESCryptoServiceProvider();
			enc.IV = iv;
			enc.Key = key;
			enc.Mode = CipherMode.CBC;
			enc.Padding = PaddingMode.PKCS7;
			ICryptoTransform transformer = enc.CreateEncryptor();
			byte[] resultArray = transformer.TransformFinalBlock(plain, 0, plain.Length);
			
			return resultArray;
		}
		
		
		void Button1Click(object sender, EventArgs e)
		{
			Random r = new Random();
			String str = "";
			String alphabet = "0123456789abcdef";
			for(int i=0; i < 16; i++)
			{
				int idx = r.Next(16);
				str += alphabet[idx];
			}
			
			txtIv.Text = str;
		}
	}
}
