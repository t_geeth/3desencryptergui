﻿/*
 * Created by SharpDevelop.
 * User: geetht
 * Date: 2018-08-09
 * Time: 2:52 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace TDesEncrypter
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtPlain;
		private System.Windows.Forms.TextBox txtIv;
		private System.Windows.Forms.TextBox txtKey;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button encrypt;
		private System.Windows.Forms.TextBox txtEnc;
		private System.Windows.Forms.TextBox txtCombined;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtEncB64;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtPlain = new System.Windows.Forms.TextBox();
			this.txtIv = new System.Windows.Forms.TextBox();
			this.txtKey = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.encrypt = new System.Windows.Forms.Button();
			this.txtEnc = new System.Windows.Forms.TextBox();
			this.txtCombined = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.txtEncB64 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(13, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(54, 18);
			this.label1.TabIndex = 0;
			this.label1.Text = "Input";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(13, 44);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(54, 18);
			this.label2.TabIndex = 1;
			this.label2.Text = "IV";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(13, 71);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(54, 18);
			this.label3.TabIndex = 2;
			this.label3.Text = "Key";
			// 
			// txtPlain
			// 
			this.txtPlain.Location = new System.Drawing.Point(63, 10);
			this.txtPlain.Name = "txtPlain";
			this.txtPlain.Size = new System.Drawing.Size(320, 20);
			this.txtPlain.TabIndex = 3;
			// 
			// txtIv
			// 
			this.txtIv.Location = new System.Drawing.Point(63, 41);
			this.txtIv.MaxLength = 16;
			this.txtIv.Name = "txtIv";
			this.txtIv.Size = new System.Drawing.Size(218, 20);
			this.txtIv.TabIndex = 4;
			// 
			// txtKey
			// 
			this.txtKey.Location = new System.Drawing.Point(63, 68);
			this.txtKey.MaxLength = 48;
			this.txtKey.Name = "txtKey";
			this.txtKey.Size = new System.Drawing.Size(320, 20);
			this.txtKey.TabIndex = 5;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(300, 35);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(66, 30);
			this.button1.TabIndex = 6;
			this.button1.Text = "generate";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// encrypt
			// 
			this.encrypt.Location = new System.Drawing.Point(13, 110);
			this.encrypt.Name = "encrypt";
			this.encrypt.Size = new System.Drawing.Size(75, 45);
			this.encrypt.TabIndex = 7;
			this.encrypt.Text = "Encrypt";
			this.encrypt.UseVisualStyleBackColor = true;
			this.encrypt.Click += new System.EventHandler(this.EncryptClick);
			// 
			// txtEnc
			// 
			this.txtEnc.Location = new System.Drawing.Point(143, 110);
			this.txtEnc.Name = "txtEnc";
			this.txtEnc.ReadOnly = true;
			this.txtEnc.Size = new System.Drawing.Size(506, 20);
			this.txtEnc.TabIndex = 8;
			// 
			// txtCombined
			// 
			this.txtCombined.Location = new System.Drawing.Point(143, 136);
			this.txtCombined.Name = "txtCombined";
			this.txtCombined.ReadOnly = true;
			this.txtCombined.Size = new System.Drawing.Size(506, 20);
			this.txtCombined.TabIndex = 9;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(95, 113);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(42, 23);
			this.label4.TabIndex = 10;
			this.label4.Text = "Enc";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(95, 136);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(42, 23);
			this.label5.TabIndex = 11;
			this.label5.Text = "IV;Enc";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(390, 10);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(259, 79);
			this.label6.TabIndex = 12;
			this.label6.Text = ">Input field is treated as text(utf-8)\r\n>Other fields are hexa\r\n> Encryption meth" +
	"od:\r\n3DES, CBC mode. PKCS7 padding";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(95, 161);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(69, 23);
			this.label7.TabIndex = 14;
			this.label7.Text = "Enc(base64)";
			// 
			// txtEncB64
			// 
			this.txtEncB64.Location = new System.Drawing.Point(170, 161);
			this.txtEncB64.Name = "txtEncB64";
			this.txtEncB64.ReadOnly = true;
			this.txtEncB64.Size = new System.Drawing.Size(479, 20);
			this.txtEncB64.TabIndex = 13;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(661, 184);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.txtEncB64);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtCombined);
			this.Controls.Add(this.txtEnc);
			this.Controls.Add(this.encrypt);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.txtKey);
			this.Controls.Add(this.txtIv);
			this.Controls.Add(this.txtPlain);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "3Des Encrypter";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
